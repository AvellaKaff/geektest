package WEB.MyPosts;

import WEB.GeekTestPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.SneakyThrows;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Страница с моими постами")
public class MyPosts {
    WebDriver driver;
    GeekTestPage geekTestPage;

    @BeforeAll
    static void registerDriver() {
        WebDriverManager.chromedriver().setup();
    }


    @BeforeEach
    void newDriver() {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        driver = new ChromeDriver(options);

//        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));


        driver.get("https://test-stand.gb.ru/login");

        geekTestPage = new GeekTestPage(driver);

        geekTestPage.loginEstudiante();
    }

    @AfterEach
    void quit() {
        driver.quit();
    }

    @SneakyThrows
    @Test
    @DisplayName("URL после авторизации")
    public void authorizationTest() {
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/", driver.getCurrentUrl());
    }

    @Test
    @DisplayName("Наличие постов")
    void postTest() {
        assertTrue(geekTestPage.getPost1().isDisplayed());
        assertTrue(geekTestPage.getPost2().isDisplayed());
        assertTrue(geekTestPage.getPost3().isDisplayed());
        assertTrue(geekTestPage.getPost4().isDisplayed());
    }

    @Test
    @DisplayName("Наличие заголовков постов")
    public void postsTitleTest() {
        assertTrue(geekTestPage.getPost1_title().isDisplayed());
        assertTrue(geekTestPage.getPost2_title().isDisplayed());
        assertTrue(geekTestPage.getPost3_title().isDisplayed());
        assertTrue(geekTestPage.getPost4_title().isDisplayed());
    }

    @Test
    @DisplayName("Наличие описания постов")
    public void postsDescriptionTest() {
        assertTrue(geekTestPage.getPost1_description().isDisplayed());
        assertTrue(geekTestPage.getPost2_description().isDisplayed());
        assertTrue(geekTestPage.getPost3_description().isDisplayed());
        assertTrue(geekTestPage.getPost4_description().isDisplayed());
    }

    @Test
    @DisplayName("Наличие изоброжений постов")
    public void postsImgTest() {
        assertTrue(geekTestPage.getPost1_img().isDisplayed());
        assertTrue(geekTestPage.getPost2_img().isDisplayed());
        assertTrue(geekTestPage.getPost3_img().isDisplayed());
        assertTrue(geekTestPage.getPost4_img().isDisplayed());
    }

    @SneakyThrows
    @Test
    @DisplayName("Заглушка при отсутствии изображания")
    void imgPlaceholderTest() {
        geekTestPage.nextPage();
        Thread.sleep(2000);
        assertTrue(geekTestPage.getPost1_img().isDisplayed());
        assertEquals("https://test-stand.gb.ru/placeholder/800x600.gif", geekTestPage.getPost1_img_src());
    }

    @SneakyThrows
    @Test
    @DisplayName("Соотношение сторон изображений 2:3")
    public void postAspectRatioTest() {
        Thread.sleep(2000);
        driver.get("https://test-stand.gb.ru/?page=3");
        assertEquals("0.6666666666666667", geekTestPage.getImg1AspectRatio());
        assertEquals("0.6666666666666667", geekTestPage.getImg2AspectRatio());
        assertEquals("0.6666666666666667", geekTestPage.getImg3AspectRatio());
        assertEquals("0.6666666666666667", geekTestPage.getImg4AspectRatio());
    }

    @SneakyThrows
    @Test
    @DisplayName("Следующая страница")
    public void nextPageTest() {
        geekTestPage.nextPage();
        Thread.sleep(3000);
        assertEquals("https://test-stand.gb.ru/?page=2", driver.getCurrentUrl());
        geekTestPage.nextPage();
        Thread.sleep(3000);
        assertEquals("https://test-stand.gb.ru/?page=3", driver.getCurrentUrl());
    }

    @SneakyThrows
    @Test
    @DisplayName("Предыдущая страница на первой странице")
    public void firstPageTest() {
        geekTestPage.previousPage();
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/", driver.getCurrentUrl());
    }

    @SneakyThrows
    @Test
    @DisplayName("Предыдущая страница")
    public void previousPageTest() {
        Thread.sleep(3000);
        driver.get("https://test-stand.gb.ru/?page=3");
        Thread.sleep(3000);
        geekTestPage.previousPage();
        Thread.sleep(3000);
        assertEquals("https://test-stand.gb.ru/?page=2", driver.getCurrentUrl());
        Thread.sleep(3000);
        geekTestPage.previousPage();
        Thread.sleep(3000);
        assertEquals("https://test-stand.gb.ru/?page=1", driver.getCurrentUrl());
    }

    @SneakyThrows
    @Test
    @DisplayName("Следующая страница на последней странице")
    public void lastPageTest() {
        Thread.sleep(3000);
        driver.get("https://test-stand.gb.ru/?page=3");
        Thread.sleep(3000);
        geekTestPage.nextPage();
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/?page=3", driver.getCurrentUrl());
    }

    @SneakyThrows
    @Test
    @DisplayName("Смена сортировки")
    void orderTest() {
        geekTestPage.change_order();
        Thread.sleep(3000);
        assertEquals("https://test-stand.gb.ru/?sort=createdAt&order=DESC", driver.getCurrentUrl());
        geekTestPage.change_order();
        Thread.sleep(3000);
        assertEquals("https://test-stand.gb.ru/?sort=createdAt&order=ASC", driver.getCurrentUrl());
    }

    @SneakyThrows
    @Test
    @DisplayName("Сортировка по-умолчанию")
    void orderDefaultTest() {
        Thread.sleep(2000);
        driver.get("https://test-stand.gb.ru/?sort=createdAt&order=ASC");
        String firstImgSrc = geekTestPage.getPost1_img_src();
        geekTestPage.clickHome();
        Thread.sleep(2000);
        assertEquals(firstImgSrc, geekTestPage.getPost1_img_src());
    }

    @SneakyThrows
    @Test
    @DisplayName("Переход на чужие посты")
    public void NotMyPostsTest() {
        geekTestPage.switchToNotMtPosts();
        Thread.sleep(2000);
        assertEquals("https://test-stand.gb.ru/?owner=notMe&sort=createdAt&order=ASC", driver.getCurrentUrl());
    }

    @Test
    @DisplayName("Кнопка Home ведет на главную страницу")
    public void homeTest() {
        geekTestPage.clickHome();
        assertEquals("Blog", geekTestPage.getTitle_Blog());
    }

}
