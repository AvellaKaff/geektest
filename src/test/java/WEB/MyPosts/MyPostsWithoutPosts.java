package WEB.MyPosts;

import WEB.GeekTestPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.SneakyThrows;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Страница с моими постами (без постов)")
public class MyPostsWithoutPosts {
    WebDriver driver;
    GeekTestPage geekTestPage;

    @BeforeAll
    static void registerDriver() {
        WebDriverManager.chromedriver().setup();
    }


    @BeforeEach
    void newDriver() {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        driver = new ChromeDriver(options);

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        driver.get("https://test-stand.gb.ru/login");

        geekTestPage = new GeekTestPage(driver);

        geekTestPage.loginValid1();
    }

    @AfterEach
    void quit() {
        driver.quit();
    }

    @SneakyThrows
    @Test
    @DisplayName("URL после авторизации")
    public void authorizationTest() {
        Thread.sleep(2000);
        assertEquals("https://test-stand.gb.ru/", driver.getCurrentUrl());
    }

    @Test
    @DisplayName("Сообщение об отсутствии постов")
    void postsTest() {
       assertEquals("No items for your filter", geekTestPage.getMessage());
    }

    @SneakyThrows
    @Test
    @DisplayName("Переход на чужие посты")
    public void NotMyPostsTest() {
        geekTestPage.switchToNotMtPosts();
        Thread.sleep(2000);
        assertEquals("https://test-stand.gb.ru/?owner=notMe&sort=createdAt&order=ASC", driver.getCurrentUrl());
    }

    @Test
    @DisplayName("Кнопка Home ведет на главную страницу")
    public void homeTest() {
        geekTestPage.clickHome();
        assertEquals("Blog", geekTestPage.getTitle_Blog());
    }


}
