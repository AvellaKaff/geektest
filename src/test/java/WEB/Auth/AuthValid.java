package WEB.Auth;

import WEB.GeekTestPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.SneakyThrows;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Авторизация с валидными данными")
public class AuthValid {
    WebDriver driver;
    GeekTestPage geekTestPage;

    @BeforeAll
    static void registerDriver() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void newDriver() {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        driver = new ChromeDriver(options);

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        driver.get("https://test-stand.gb.ru/login");

        geekTestPage = new GeekTestPage(driver);

    }

    @AfterEach
    void quit() {
        driver.quit();
    }

    @SneakyThrows
    @Test
    @DisplayName("estudiante авторизация (с постами)")
    public void authorizationEstudianteTest() {
        geekTestPage.loginEstudiante();
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/" ,driver.getCurrentUrl());
        assertEquals("estudiante", geekTestPage.getLogin());
    }

    @SneakyThrows
    @Test
    @DisplayName("valid1 авторизация (без постов)")
    public void authorizationValid1Test() {
        geekTestPage.loginValid1();
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/" ,driver.getCurrentUrl());
        assertEquals("valid1", geekTestPage.getLogin());
    }

    @SneakyThrows
    @Test
    @DisplayName("Минимальное кол-во символов")
    public void authorizationMinSymbolTest() {
        geekTestPage.loginMinSymbol();
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/" ,driver.getCurrentUrl());
        assertEquals("sin", geekTestPage.getLogin());
    }

    @SneakyThrows
    @Test
    @DisplayName("Максимальное кол-во символов")
    public void authorizationMaxSymbolTest() {
        geekTestPage.loginMaxSymbol();
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/" ,driver.getCurrentUrl());
        assertEquals("qwertyuiopasdfghjklf", geekTestPage.getLogin());
    }
}
