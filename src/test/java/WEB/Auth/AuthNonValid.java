package WEB.Auth;

import WEB.GeekTestPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Авторизация с невалидными данными (проверка сообщений об ошибках)")
public class AuthNonValid {
    WebDriver driver;
    GeekTestPage geekTestPage;

    @BeforeAll
    static void registerDriver() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void newDriver() {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        driver = new ChromeDriver(options);

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        driver.get("https://test-stand.gb.ru/login");

        geekTestPage = new GeekTestPage(driver);

    }

    @AfterEach
    void quit() {
        driver.quit();
    }


    @Test
    @DisplayName("Неактивированный аккаунт")
    public void authorizationInactiveTest() {
        geekTestPage.loginInactive();
        assertEquals("Ошибка. Попробуйте еще раз позже", geekTestPage.getError_message().getText());
    }

    @Test
    @DisplayName("Русские буквы в логине (не латиница)")
    public void authorizationRusTest() {
        geekTestPage.loginRus();
        assertEquals("Неправильный логин. Может состоять только из латинских букв и цифр, без спецсимволов", geekTestPage.getError_message().getText());
    }

    @Test
    @DisplayName("Специальные символы в логине")
    public void authorizationSpecSymbolTest() {
        geekTestPage.loginSpecSymbol();
        assertEquals("Неправильный логин. Может состоять только из латинских букв и цифр, без спецсимволов", geekTestPage.getError_message().getText());
    }

    @Test
    @DisplayName("2 символа в логине (минумум 3)")
    public void authorizationlhTest() {
        geekTestPage.loginlh();
        assertEquals("Неправильный логин. Может быть не менее 3 и не более 20 символов", geekTestPage.getError_message().getText());
    }

    @Test
    @DisplayName("21 символ в логине (максимум 20)")
    public void authorization123456789012345678901Test() {
        geekTestPage.login123456789012345678901();
        assertEquals("Неправильный логин. Может быть не менее 3 и не более 20 символов", geekTestPage.getError_message().getText());
    }

    @Test
    @DisplayName("Неправильный пароль к валидному логину")
    public void authorizationestudianteNonValidTest() {
        geekTestPage.loginestudianteNonValid();
        assertEquals("Проверьте логин и пароль", geekTestPage.getError_message().getText());
    }


    @Test
    @DisplayName("Пустые поля логин и пароль")
    public void authorizationWithoutLoginAndPasswordTest() {
        geekTestPage.loginWithoutLoginAndPassword();
        assertEquals("Поле не может быть пустым", geekTestPage.getError_message().getText());
    }
}
