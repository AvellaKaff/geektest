package API.NotMyPosts;

import API.AbstractTest;
import io.restassured.http.Method;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class NotMyPostsWithoutAuth extends AbstractTest {

    @Test
    void getNoAuthTest() {
        given()
                .spec(requestSpecificationNoAuth)
                .queryParam(owner, notMe)
                .expect()
                .body("message", equalTo("Auth header required X-Auth-Token"))
                .when()
                .request(Method.GET, getUrl())
                .then()
                .spec(responseSpecificationNoAuth)
                .extract()
                .body();
    }

    @Test
    void getWithoutTokenTest() {
        given()
                .spec(requestSpecificationNoAuth)
                .queryParam(owner, notMe)
                .header(getHeader(), "")
                .expect()
                .body("message", equalTo("No API token provided or is not valid"))
                .when()
                .request(Method.GET, getUrl())
                .then()
                .spec(responseSpecificationNoAuth)
                .extract()
                .body();
    }
}
