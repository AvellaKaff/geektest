package API.NotMyPosts;

import API.AbstractTest;
import API.Posts.Posts;
import io.restassured.http.Method;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class NotMyPosts extends AbstractTest {

    @Test
    void getOwnerNotMeTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMeOwner)
                        .expect()
                        .body(containsString("data"))
                        .body(containsString("id"))
                        .body(containsString("title"))
                        .body(containsString("description"))
                        .body(containsString("content"))
                        .body(containsString("authorId"))
                        .body(containsString("mainImage"))
                        .body(containsString("cdnUrl"))
                        .body(containsString("updatedAt"))
                        .body(containsString("createdAt"))
                        .body(containsString("labels"))
                        .body(containsString("draft"))
                        .body(containsString("meta"))
                        .body(containsString("prevPage"))
                        .body(containsString("nextPage"))
                        .body(containsString("delayPublishTo"))
                        .body(containsString("count"))
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.prevPage, equalTo(1));
    }

    @Test
    void getSortCreatedAtTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMeOwner)
                        .queryParam(sort, createdAt)
                        .queryParam(page, "100")
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.get(0).id - posts.data.get(1).id, greaterThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, greaterThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, greaterThan(0));
    }

    @Test
    void getDefaultOrderTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMeOwner)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.prevPage, equalTo(1));
        assertThat(posts.data.get(0).id - posts.data.get(1).id, lessThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, lessThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, lessThan(0));
    }

    @Test
    void getOrderASCTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMeOwner)
                        .queryParam(order, ASC)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.get(0).id - posts.data.get(1).id, lessThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, lessThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, lessThan(0));
    }

    @Test
    void getOrderDESCTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMeOwner)
                        .queryParam(order, DESC)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.get(0).id - posts.data.get(1).id, greaterThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, greaterThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, greaterThan(0));
    }

    @Test
    void getOrderAllTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMeOwner)
                        .queryParam(order, ALL)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
    }

    @Test
    void getPage0Test() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMeOwner)
                        .queryParam(page, "0")
                        .expect()
                        .body(containsString("title"))
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
    }

    @Test
    void getPage1Test() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMeOwner)
                        .queryParam(sort, createdAt)
                        .queryParam(order, ASC)
                        .queryParam(page, "1")
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.nextPage, equalTo(2));
    }

    @Test
    void getPage500Test() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMeOwner)
                        .queryParam(order, DESC)
                        .queryParam(page, "500")
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.prevPage, equalTo(499));
        assertThat(posts.meta.nextPage, equalTo(501));
    }


    @Test
    void getNon_existentPageTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMeOwner)
                        .queryParam(sort, createdAt)
                        .queryParam(page, "11800")
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.size(), equalTo(0));
        assertThat(posts.meta.nextPage, equalTo(null));
    }
}
