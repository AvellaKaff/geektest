package API.Auth.valid;

import API.AbstractTest;
import API.Auth.Authorization;
import com.google.gson.Gson;
import io.restassured.http.Method;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class Auth extends AbstractTest {

    Gson g = new Gson();

    @Test
    void postAuthEstudianteTest() {
        String s =
                given()
                        .formParam(username, "estudiante")
                        .formParam(password, "e4e4564027")
                        .request(Method.POST, getUrlLogin())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .asString();
        Authorization authorization = g.fromJson(s, Authorization.class);
        System.out.println(authorization.toString());
        assertThat(authorization.username , equalTo("estudiante"));
    }

    @Test
    void postAuthValidTest() {
        String s =
                given()
                        .formParam(username, "valid1")
                        .formParam(password, "958af151b6")
                        .request(Method.POST, getUrlLogin())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .asString();
        Authorization authorization = g.fromJson(s, Authorization.class);
        System.out.println(authorization.toString());
        assertThat(authorization.username, equalTo("valid1"));
    }

    @Test
    void postAuthMinSymbolTest() {
        String s =
                given()
                        .formParam(username, "sin")
                        .formParam(password, "7d27e4a7ca")
                        .request(Method.POST, getUrlLogin())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .asString();
        Authorization authorization = g.fromJson(s, Authorization.class);
        System.out.println(authorization.toString());
        assertThat(authorization.username, equalTo("sin"));
    }

    @Test
    void postAuthMaxSymbolTest() {
        String s =
                given()
                        .formParam(username, "qwertyuiopasdfghjklf")
                        .formParam(password, "06928567b7")
                        .request(Method.POST, getUrlLogin())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .asString();
        Authorization authorization = g.fromJson(s, Authorization.class);
        System.out.println(authorization.toString());
        assertThat(authorization.username, equalTo("qwertyuiopasdfghjklf"));
    }



}
