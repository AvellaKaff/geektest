package API.Auth.nonValid;

import API.AbstractTest;
import io.restassured.http.Method;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class Auth  extends AbstractTest {

    @Test
    void postAuthWithoutLoginAndPasswordTest() {
        given()
                .formParam(username, "")
                .formParam(password, "")
                .request(Method.POST, getUrlLogin())
                .then()
                .spec(responseSpecificationNoAuth)
                .extract()
                .body();
    }

    @Test
    void postAuthInactiveTest() {
        given()
                .formParam(username, "inactive")
                .formParam(password, "19d3894f53")
                .request(Method.POST, getUrlLogin())
                .then()
                .spec(responseSpecificationNoAuth)
                .extract()
                .body();
    }

    @Test
    void postAuthRusTest() {
                given()
                        .formParam(username, "русский")
                        .formParam(password, "3495b78aae")
                        .request(Method.POST, getUrlLogin())
                        .then()
                        .spec(responseSpecificationNoAuth)
                        .extract()
                        .body();
    }

    @Test
    void postAuthSpecSymbolTest() {
                given()
                        .formParam(username, "!@#")
                        .formParam(password, "5041175879")
                        .request(Method.POST, getUrlLogin())
                        .then()
                        .spec(responseSpecificationNoAuth)
                        .extract()
                        .body();
    }

    @Test
    void postAuthlhTest() {
                given()
                        .formParam(username, "lh")
                        .formParam(password, "8ecc6960ab")
                        .request(Method.POST, getUrlLogin())
                        .then()
                        .spec(responseSpecificationNoAuth)
                        .extract()
                        .body();
    }

    @Test
    void postAuth123456789012345678901Test() {
                given()
                        .formParam(username, "123456789012345678901")
                        .formParam(password, "b7d8a4d271")
                        .request(Method.POST, getUrlLogin())
                        .then()
                        .spec(responseSpecificationNoAuth)
                        .extract()
                        .body();
    }

    @Test
    void postAuthEstudianteTest() {
                given()
                        .formParam(username, "estudiante")
                        .formParam(password, "неверныйПароль")
                        .request(Method.POST, getUrlLogin()).prettyPeek()
                        .then()
                        .spec(responseSpecificationNoAuth)
                        .extract()
                        .body();
    }
}
