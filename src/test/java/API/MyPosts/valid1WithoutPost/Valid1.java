package API.MyPosts.valid1WithoutPost;

import API.AbstractTest;
import API.Posts.Posts;
import io.restassured.http.Method;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class Valid1 extends AbstractTest {
    @Test
    void getMyPostsTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationValid1)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.nextPage, equalTo(null));
        assertThat(posts.data.size(), equalTo(0));
    }
}
