package API.MyPosts.estudiante;

import API.AbstractTest;
import API.Posts.Posts;
import io.restassured.http.Method;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class Estudiante extends AbstractTest {

//    @BeforeEach
//    void specification() {
//        RestAssured.requestSpecification = requestSpecificationEstudiante;
//        RestAssured.responseSpecification = responseSpecification;
//    }

    @Test
    void getMyPostsTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationEstudiante)
                        .expect()
                        .body(containsString("data"))
                        .body(containsString("id"))
                        .body(containsString("title"))
                        .body(containsString("description"))
                        .body(containsString("content"))
                        .body(containsString("authorId"))
                        .body(containsString("mainImage"))
                        .body(containsString("cdnUrl"))
                        .body(containsString("updatedAt"))
                        .body(containsString("createdAt"))
                        .body(containsString("labels"))
                        .body(containsString("draft"))
                        .body(containsString("meta"))
                        .body(containsString("prevPage"))
                        .body(containsString("nextPage"))
                        .body(containsString("delayPublishTo"))
                        .body(containsString("count"))
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.prevPage, equalTo(1));
        assertThat(posts.data.get(0).authorId, equalTo(getId_estudiante()));
    }


    @Test
    void getSortCreatedAtTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationEstudiante)
                        .queryParam(sort, createdAt)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.get(0).id - posts.data.get(1).id, greaterThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, greaterThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, greaterThan(0));
    }

    @Test
    void getDefaultOrderTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationEstudiante)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.prevPage, equalTo(1));
        assertThat(posts.data.get(0).id - posts.data.get(1).id, lessThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, lessThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, lessThan(0));
    }

    @Test
    void getOrderASCTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationEstudiante)
                        .queryParam(order, ASC)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.get(0).id - posts.data.get(1).id, lessThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, lessThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, lessThan(0));
    }

    @Test
    void getOrderDESCTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationEstudiante)
                        .queryParam(order, DESC)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.get(0).id - posts.data.get(1).id, greaterThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, greaterThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, greaterThan(0));
    }

    @Test
    void getPage0Test() {
        Posts posts =
                given()
                        .spec(requestSpecificationEstudiante)
                        .queryParam(order, ASC)
                        .queryParam(page, "0")
                        .expect()
                        .body(containsString("title"))
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.prevPage, equalTo(1));
    }

    @Test
    void getPage1Test() {
        Posts posts =
                given()
                        .spec(requestSpecificationEstudiante)
                        .queryParam(sort, createdAt)
                        .queryParam(order, DESC)
                        .queryParam(page, "1")
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.prevPage, equalTo(1));
    }

    @Test
    void getPage2Test() {
        Posts posts =
                given()
                        .spec(requestSpecificationEstudiante)
                        .queryParam(sort, createdAt)
                        .queryParam(page, "2")
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.prevPage, equalTo(1));
    }

    @Test
    void getPage3Test() {
        Posts posts =
                given()
                        .spec(requestSpecificationEstudiante)
                        .queryParam(page, "3")
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.prevPage, equalTo(2));
    }

    @Test
    void getNon_existentPageTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationEstudiante)
                        .queryParam(sort, createdAt)
                        .queryParam(page, "11800")
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.size(), equalTo(0));
        assertThat(posts.meta.nextPage, equalTo(null));
    }
}
